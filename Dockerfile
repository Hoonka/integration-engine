FROM bitnami/node:16.17.0 AS build

# FAKETIME
#USER root
RUN apt update && \
    apt install -y \
        git \
        make \
        build-essential && \
    rm -r /var/lib/apt/lists /var/cache/apt/archives

RUN git clone https://github.com/wolfcw/libfaketime.git && \
    cd libfaketime/src && \
    make install

# JS BUILD
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build-minimal


FROM bitnami/node:16.17.0
WORKDIR /app

COPY --from=build /usr/local/lib/faketime/libfaketime.so.1 /usr/local/lib
COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/dist /app/dist
COPY --from=build /app/test /app/test
COPY config config
COPY tsconfig.json ./
COPY package.json ./

# Create a non-root user
RUN groupadd --system nonroot &&\
    useradd --system --base-dir /app --uid 1001 --gid nonroot nonroot && \
    chown -R nonroot /app
USER nonroot

EXPOSE 3006
CMD ["node", "-r",  "dotenv/config", "dist/index.js"]

# For FAKETIME use prefix like:
# LD_PRELOAD=/usr/local/lib/libfaketime.so.1 FAKETIME="@2022-02-22 20:22:00" date
# LD_PRELOAD=/usr/local/lib/libfaketime.so.1 FAKETIME="@2022-02-22 20:22:00" yarn start
