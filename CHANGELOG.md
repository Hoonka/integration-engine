# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.46] - 2022-10-05

No changelog

## [2.0.45] - 2022-10-04

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))

### Removed

-   Datasource tests ([pid#174](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/174))
-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))
